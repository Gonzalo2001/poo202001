package abstracto1;

public class Inicio {
    public static void main(String[] args) {
        Reloj a = new RelojAutomatico();
        for (int i = 0; i < 20 ; i++) {
            a.incrementar();
        }
        System.out.println("Inicio: "+a.getInicio());
        Reloj b = new RelojNuclear();
        for (int i = 0; i < 20 ; i++) {
            b.incrementar();
        }
        System.out.println("Inicio: "+b.getInicio());
    }
}
