package abstracto1;

public abstract class Reloj {
    private int inicio;
    public Reloj() {
        System.out.println("Reloj sin argumentos");
        this.inicio = 0;
    }

    public Reloj(int inicio) {
        System.out.println("Reloj int");
        this.inicio = inicio;
    }

    public abstract void incrementar();

    public int getInicio() {
        return inicio;
    }

    public void setInicio(int inicio) {
        this.inicio = inicio;
    }
}

