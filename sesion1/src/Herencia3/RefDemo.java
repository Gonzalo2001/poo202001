package Herencia3;

public class RefDemo {
    public static void main(String[] args) {
        BoxWeight weightbox = new BoxWeight(3,5,7,8.37);
        Box plainbox = new Box();
        double vol;

        vol = weightbox.volumen();
        System.out.println("Volumen de weightbox es: "+vol);
        System.out.println("Weight de weightbox es: "+weightbox.peso);
        System.out.println();

        plainbox = weightbox;
        vol = plainbox.volumen();
        System.out.println("Volumen de plainbox es: "+vol);
       // System.out.println("Weight de plainbox es: "+plainbox.peso);
    }
}
