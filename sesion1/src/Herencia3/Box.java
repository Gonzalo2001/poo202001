package Herencia3;

public class Box {
    double ancho;
    double altura;
    double profundidad;

    public Box(Box ob){
        ancho = ob.ancho;
        altura = ob.altura;
        profundidad = ob.profundidad;
    }

    public Box(double w,double h,double d){
        ancho = w;
        altura = h;
        profundidad = d;
    }

    public Box() {
        ancho = -1;
        altura = -1;
        profundidad = -1;
    }

    public Box(double len){
        ancho = altura = profundidad = len;
    }

    double volumen(){
        return ancho*altura*profundidad;
    }
}