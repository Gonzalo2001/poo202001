package Herencia3;

public class DemoBoxWeight {
    public static void main(String[] args) {
        BoxWeight mybox1 = new BoxWeight(10,20,15,34.3);
        BoxWeight mybox2 = new BoxWeight(2,3,4,0.076);
        ColorBox mybox3 = new ColorBox(3,5,8,2);
        double vol;

        vol = mybox1.volumen();
        System.out.println("Volumen de mybox1 es: "+ vol);
        System.out.println("Peso de mybox1 es: "+ mybox1.peso);
        System.out.println();

        vol = mybox2.volumen();
        System.out.println("Volumen de mybox2 es: "+ vol);
        System.out.println("Peso de mybox2 es: "+ mybox2.peso);
        System.out.println();

        vol = mybox3.volumen();
        System.out.println("Volumen de mybox3 es: "+ vol);
        System.out.println("Color de mybox3 es: "+ mybox3.color);
        System.out.println();
    }
}
