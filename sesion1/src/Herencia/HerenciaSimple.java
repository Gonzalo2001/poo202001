package Herencia;

public class HerenciaSimple {
    public static void main(String[] args) {
        A superOb = new A();
        B subOb = new B();

        superOb.i=10;
        superOb.j=20;
        System.out.println("Contenido de superOb: ");
        superOb.showij();
        System.out.println();

        subOb.i = 7;
        subOb.j = 8;
        subOb.k = 9;
        System.out.println("Contenido de subOb: ");
        subOb.showk();
        System.out.println();

        System.out.println("Suma de i,j y k in subOb: ");
        subOb.sum();
    }
}
