package Herencia4;

public class Box {
    private double ancho;
    private double altura;
    private double profundidad;

    public Box(Box ob) {
        ancho = ob.ancho;
        altura = ob.altura;
        profundidad = ob.profundidad;
    }

    public Box(double w, double h, double d) {
        ancho = w;
        altura = h;
        profundidad = d;
    }

    public Box() {
        ancho = -1;
        altura = -1;
        profundidad = -1;
    }

    public Box(double len){
        ancho = altura = profundidad = len;
    }

    double volume(){
        return ancho*altura*profundidad;
    }
}
