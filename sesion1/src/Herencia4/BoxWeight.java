package Herencia4;

public class BoxWeight extends Box{
    double peso;

    public BoxWeight(BoxWeight ob){
        super(ob);
        peso = ob.peso;
    }

    public BoxWeight(double w, double h, double d, double m){
        super(w, h, d);
        peso = m;
    }

    public BoxWeight(){
        super();
        peso = -1;
    }

    public BoxWeight(double len,double m){
        super(len);
        peso = m;
    }

}
