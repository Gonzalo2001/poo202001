package Herencia4;

public class DemoSuper{
    public static void main(String[] args) {
        BoxWeight mybox1 = new BoxWeight(10,20,15,34.3);
        BoxWeight mybox2 = new BoxWeight(2,3,4,0.076);
        BoxWeight mybox3 = new BoxWeight();
        BoxWeight mycube = new BoxWeight(3,2);
        BoxWeight myclone = new BoxWeight(mybox1);
        double vol;

        vol = mybox1.volume();
        System.out.println("El volumen de la caja 1 es: "+ vol);
        System.out.println("El peso de la caja 1 es: "+mybox1.peso);
        System.out.println();

        vol = mybox2.volume();
        System.out.println("El volumen de la caja 2 es: "+ vol);
        System.out.println("El peso de la caja 2 es: "+mybox2.peso);
        System.out.println();

        vol = mybox3.volume();
        System.out.println("El volumen de la caja 3 es: "+ vol);
        System.out.println("El peso de la caja 3 es: "+mybox3.peso);
        System.out.println();

        vol = mycube.volume();
        System.out.println("El volumen del cubo es: "+ vol);
        System.out.println("El peso del cubo es: "+mycube.peso);
        System.out.println();

        vol = myclone.volume();
        System.out.println("El volumen de la caja 1 es: "+ vol);
        System.out.println("El peso de la caja 1 es: "+myclone.peso);
    }
}
