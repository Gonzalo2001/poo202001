package Herencia10;
//abstract
public  class Figura {
    double dim1;
    double dim2;

    public Figura(double a, double b){
        dim1 = a;
        dim2 = b;
    }

    double area(){
        System.out.println("El area para la figura es indefinida");
        return  0;
    }
}
