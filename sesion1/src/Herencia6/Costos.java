package Herencia6;

public class Costos extends BoxWeight{
    double costo;

    public Costos(Costos ob){
        super(ob);
        costo = ob.costo;
    }

    public Costos(double w, double h, double d, double p, double c){
        super(w, h, d, p);
        costo = c;
    }

    public Costos(){
        super();
        costo = -1;
    }

    public Costos(double len, double p, double c){
        super(len, p);
        costo = c;
    }
}
