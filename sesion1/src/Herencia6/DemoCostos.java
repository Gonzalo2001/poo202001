package Herencia6;

public class DemoCostos {
    public static void main(String[] args) {
        Costos Costo1 = new Costos(10,20,15,10,3.41);
        Costos Costo2 = new Costos(2,3,4,0.76,1.28);
        double vol;

        vol = Costo1.volume();
        System.out.println("El volumen de la caja 1 es: "+ vol);
        System.out.println("El peso de la caja 1 es: "+Costo1.peso);
        System.out.println("El precio de la caja 1 es: $"+Costo1.costo);
        System.out.println();

        vol = Costo2.volume();
        System.out.println("El volumen de la caja 2 es: "+ vol);
        System.out.println("El peso de la caja 2 es: "+Costo2.peso);
        System.out.println("El precio de la caja 2 es: $"+Costo2.costo);
        System.out.println();

    }
}
