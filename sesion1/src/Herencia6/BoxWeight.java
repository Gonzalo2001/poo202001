package Herencia6;

public class BoxWeight extends Box{
    double peso;

    public BoxWeight(BoxWeight ob){
        super(ob);
        peso = ob.peso;
    }

    public BoxWeight(double w,double h,double d, double p){
        super(w, h, d);
        peso = p;
    }

    public BoxWeight(){
        super();
        peso = -1;
    }

    public BoxWeight(double len, double p){
        super(len);
        peso = p;
    }
}
