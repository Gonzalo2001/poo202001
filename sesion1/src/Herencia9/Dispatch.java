package Herencia9;

public class Dispatch {
    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        C c = new C();

        A r;

        r = a;
        r.llamar();

        r = b;
        r.llamar();

        r = c;
        r.llamar();
    }
}
